package calculator;

public enum Operator {
    ADD(new Adder()), SUBTRACT(new Subtractor()), MULTIPLY(new Multiplier());

    private OperationHandler handler;

    Operator(OperationHandler handler) {
        this.handler = handler;
    }

    public int execute(int a, int b) {
        return handler.calculate(a, b);
    }
}
