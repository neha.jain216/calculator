package calculator;

public class Subtractor implements OperationHandler {
    @Override
    public int calculate(int a, int b) {
        return a-b;
    }
}
