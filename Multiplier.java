package calculator;

public class Multiplier implements OperationHandler {
    @Override
    public int calculate(int a, int b) {
        return a*b;
    }
}
