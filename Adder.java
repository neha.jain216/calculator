package calculator;

public class Adder implements OperationHandler {
    @Override
    public int calculate(int a, int b) {
        return a+b;
    }
}
