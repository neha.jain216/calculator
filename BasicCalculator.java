package calculator;

public class BasicCalculator {

    public int calculate(Operator operator, int a, int b) {
        return operator.execute(a,b);
    }

}
