package calculator;

public interface OperationHandler {
    int calculate(int a, int b);
}
