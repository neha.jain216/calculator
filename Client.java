package calculator;

public class Client {
    public static void main(String[] args) {
        BasicCalculator calculator = new BasicCalculator();

        int sum = calculator.calculate(Operator.ADD, 2, 3);
        System.out.println(sum);

        int bal = calculator.calculate(Operator.SUBTRACT, 5, 2);
        System.out.println(bal);

        int mul = calculator.calculate(Operator.MULTIPLY, 5, 2);
        System.out.println(mul);

    }
}
